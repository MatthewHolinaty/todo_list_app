package bloo.bleh.blah.todolist;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class ToDoListActivity extends AppCompatActivity {

    // ArrayAdapter for the ToDoList View
    private ArrayAdapter<String> toDoAdapter;

    // ListView used to display the user's ToDoList
    private ListView toDoListView;

    // List of Strings representing the tasks the user has entered into their ToDoList
    private ArrayList<String> toDoList;

    // AlertDialog Builder responsible for creating the Edit Task dialog the user will use to edit
    // an existing task
    private AlertDialog.Builder editDialogBuilder;

    // AlertDialog Builder responsible for creating the Duplicate Entry dialog the user will encounter
    // when adding a duplicate entry to the to do list
    private AlertDialog.Builder duplicateEntryDialogBuilder = null;

    // AlertDialog that will appear when the user attempts to add a duplicate entry to the to do list
    private AlertDialog duplicateEntryDialog = null;

    // AlertDialog Builder responsible for creating the Duplicate Entry dialog the user will encounter
    // when adding a empty entry to the to do list
    private AlertDialog.Builder emptyEntryDialogBuilder = null;

    // AlertDialog that will appear when the user attempts to add an empty entry to the to do list
    private AlertDialog emptyEntryDialog = null;

    // Local storage shared preferences that stores to do list entries
    private SharedPreferences prefs;

    // String representing the shared preference file location for to do list entries
    private static final String TODO_LIST_SHARED_PREFS_FILE = "definitely_not_the_todo_list_shared_preference_file";

    // Constants for Error Dialogs
    private static final String EMPTY_ENTRY_DIALOG_TITLE = "EMPTY TASK ERROR";
    private static final String EMPTY_ENTRY_DIALOG_MESSAGE = "Cannot add empty task to list.";
    private static final String DUPLICATE_ENTRY_DIALOG_TITLE = "DUPLICATE TASK ERROR";
    private static final String DUPLICATE_ENTRY_DIALOG_MESSAGE = "That task already exists!";


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);

        initToDoObjects();
        updateToDoUI();
    }

    /**
     * Initializes to do list objects
     * */
    private void initToDoObjects(){
        toDoListView = findViewById(R.id.to_do_list);

        prefs = getSharedPreferences(TODO_LIST_SHARED_PREFS_FILE, Context.MODE_PRIVATE);
        String todo_list_entries_csv = prefs.getString("todo_list", null);

        if (todo_list_entries_csv != null){
            String[] todo_list_entries_array = todo_list_entries_csv.split(",");
            toDoList = new ArrayList<>(Arrays.asList(todo_list_entries_array));
        }
        else
            toDoList = new ArrayList<>();

    }



    /**
     * Notifies the to do adapter that the state of its data has changed.
     * Lazily loads the to do adapter if it hasn't been initialized yet.
     * */
    private void updateToDoUI(){
        if (toDoAdapter == null){
            toDoAdapter = new ArrayAdapter<>(this, R.layout.list_entry_layout, R.id.entryText, toDoList);
            toDoListView.setAdapter(toDoAdapter);
        }
        else{
            toDoAdapter.notifyDataSetChanged();
            String todo_list_entries_csv = android.text.TextUtils.join(",", toDoList);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("todo_list", todo_list_entries_csv);
            editor.apply();
        }
    }

    /**
     * Adds a task entry to the list
     *
     * @param view source of this function call
     * */
    public void addToDoEntry(final View view){
        View parent = (View) view.getParent();
        final EditText editText = parent.findViewById(R.id.list_entry_text_field);
        final String toDoEntry = editText.getText().toString();

        editText.getText().clear();

        if (toDoEntry.isEmpty()){
            displayEmptyEntryDialog();
            return;
        }
        else if (toDoList.contains(toDoEntry)){
            displayDuplicateEntryDialog();
            return;
        }

        toDoList.add(toDoEntry);
        updateToDoUI();
    }

    /**
     * Prompts the user with a popup to edit an existing task entry
     *
     * @param view source of this function call
     * */
    public void editToDoEntry(final View view){
        final View parent = (View) view.getParent();
        final TextView currentTextView = parent.findViewById(R.id.entryText);
        final EditText editText = new EditText(this);

        editText.setText(currentTextView.getText());

        updateEditDialogBuilder(currentTextView, editText);

        AlertDialog dialog = editDialogBuilder.create();
        dialog.show();
    }

    /**
     * Removes a task entry from the list
     *
     * @param view source of this function call
     * */
    public void deleteToDoEntry(final View view){
        final View parent = (View) view.getParent();
        final TextView textView = parent.findViewById(R.id.entryText);
        final String task = textView.getText().toString();

        toDoList.remove(task);
        updateToDoUI();
    }

    /**
     * Updates whether or not the task will have the STRIKE_THRU format via whether or not the
     * task's corresponding CheckBox is checked.
     * If checked the task will be considered 'complete' and the text will be striked.
     * If unchecked the task's text will be un-striked.
     *
     * @param view source of this function call
     * */
    public void onToDoEntryCheckBoxClicked(final View view) {
        final View parent = (View) view.getParent();
        final CheckBox checkBox = parent.findViewById(R.id.checkBox);
        final TextView textView = parent.findViewById(R.id.entryText);

        if (checkBox.isChecked())
            // Add the STRIKE_THRU flag to the text's paint flags while maintaining any other flags
            // that may be set.
            textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        else
            // Remove the STRIKE_THRU flag to the text's paint flags while maintaining any other
            // flags that may be set.
            textView.setPaintFlags(textView.getPaintFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
    }

    /**
     * Removes all task entries from the list
     *
     * @param view source of this function call
     * */
    public void clearAllToDoEntries(final View view){
        toDoAdapter.clear();
        updateToDoUI();
    }

    /**
     * Initializes the edit task dialog builder and sets properties that will be static throughout
     * runtime.
     * */
    private void initEditDialogBuilder(){
        editDialogBuilder = new AlertDialog.Builder(this);

        editDialogBuilder.setTitle(R.string.edit_task_dialog_title);
        editDialogBuilder.setNegativeButton(R.string.cancel_button_text, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialogInterface, final int i) {
                return;
            }
        });
    }

    /**
     * Updates the edit dialog builder's view attribute and positiveButton onClick actions based
     * on the given parameters. Lazily loads the edit dialog builder if it hasn't been initialized.
     *
     * @param currentTextView TextView that contains the original text and will display the
     *                        contents of the EditText object if OK is clicked
     * @param editText EditText that will be used by the user to edit the task's current text
     *                 Upon the user clicking OK the value of this object's text will be set as the
     *                 original TextView's text
     * */
    private void updateEditDialogBuilder(final TextView currentTextView, final EditText editText){
        if (editDialogBuilder == null){
            initEditDialogBuilder();
        }

        editDialogBuilder.setView(editText);

        editDialogBuilder.setPositiveButton(R.string.OK_button_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialogInterface, final  int i) {
                int currentTextViewIndex = toDoList.indexOf(currentTextView.getText().toString());
                toDoList.set(currentTextViewIndex, editText.getText().toString());
                updateToDoUI();
            }
        });

    }

    /**
     * Displays the empty entry dialog.
     * Initializes the dialog if it hasn't already been created.
     * */
    private void displayEmptyEntryDialog(){
        if (emptyEntryDialogBuilder == null || emptyEntryDialog == null){
            emptyEntryDialog = getErrorMessageDialog(emptyEntryDialog, emptyEntryDialogBuilder, EMPTY_ENTRY_DIALOG_TITLE, EMPTY_ENTRY_DIALOG_MESSAGE);
        }

        emptyEntryDialog.show();
    }

    /**
     * Displays the duplicate entry dialog.
     * Initializes the dialog if it hasn't already been created.
     * */
    private void displayDuplicateEntryDialog(){
        if (duplicateEntryDialogBuilder == null || duplicateEntryDialog == null){
            duplicateEntryDialog = getErrorMessageDialog(duplicateEntryDialog, duplicateEntryDialogBuilder, DUPLICATE_ENTRY_DIALOG_TITLE, DUPLICATE_ENTRY_DIALOG_MESSAGE);
        }

        duplicateEntryDialog.show();
    }

    /**
     * Creates a generic alert dialog that has an OK button
     *
     * @param dialog AlertDialog to initialize
     * @param builder Builder used to create the AlertDialog
     * @param title String representing the title of the dialog
     * @param message String representing the message of the dialog
     * */
    private AlertDialog getErrorMessageDialog(AlertDialog dialog, AlertDialog.Builder builder, final String title, final String message){
        builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.OK_button_text, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;
            }
        });

        dialog = builder.create();
        return dialog;
    }
}
